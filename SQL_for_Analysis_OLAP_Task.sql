SELECT
  category,
  SUM(sales_amount) AS total_sales_amount
FROM
  sales
WHERE
  sale_date BETWEEN 'start_date' AND 'end_date'
GROUP BY
  category;
  
 
 
 
 SELECT
  region,
  AVG(sales_quantity) AS average_sales_quantity
FROM
  sales
WHERE
  product_id = 'your_product_id'
GROUP BY
  region;
  
 
 
 SELECT
  customer_id,
  SUM(sales_amount) AS total_sales_amount
FROM
  sales
GROUP BY
  customer_id
ORDER BY
  total_sales_amount DESC
LIMIT 5;